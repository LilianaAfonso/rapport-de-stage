import Vue from 'vue'
import Meta from 'vue-meta'
import Mq from 'vue-mq'
// import router from './router'
import store from './store'
import App from './App'

Vue.use(Meta)

Vue.use(Mq, {
  breakpoints: {
    xs: 300,
    sm: 600,
    md: 950, // 900
    lg: 1200,
    xl: 1600 // 1800
  }
})

window.onload = () => {
  new Vue({
    // router,
    store,
    render: h => h(App)
  }).$mount('#app')
}
