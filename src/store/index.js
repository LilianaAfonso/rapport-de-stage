import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    locale: 'fr', // Default is french
    i18n: null,
    products: null
  },
  mutations: {
    storeLocale (state, lang) {
      state.locale = lang
    },
    storeI18n (state, i18n) {
      state.i18n = i18n
    }
  }
})
